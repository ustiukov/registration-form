import { Injectable } from '@angular/core';

interface dataTemplate {
  account: {
    email: string
    password: string
    confirmPassword?: string
  },
  profile: {
    userName: string
    phoneNumber: string
    city: string
  },
  company: {
    companyName: string
    propertyForm: string
    inn: string
    kpp: string
    okpo: string
    educationDate: any
  },
  contacts?: Array<{
    name: string
    jobTitle: string
    phone: string
  }>
}

@Injectable({
  providedIn: 'root'
})

export class DataManagerService {
  hasFormData: boolean = false
  formData: dataTemplate

  hasData(): Promise<boolean> {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(this.hasFormData)
      }, 100)
    })
  }
}
