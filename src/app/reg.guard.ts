import {inject} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivateFn, Router,
  RouterStateSnapshot
} from "@angular/router";
import {Observable} from "rxjs";
import {DataManagerService} from "./services/data-manager.service";

export const RegGuard: CanActivateFn = (route: ActivatedRouteSnapshot,
                                        state: RouterStateSnapshot
): Observable<boolean> | Promise<boolean> | boolean  => {
  const router = inject(Router)
  const dataManagerService = inject(DataManagerService)
  return dataManagerService.hasData().then(hasData => {
    if (hasData) return true
    else {
      dataManagerService.hasFormData = true
      router.navigate(['registration'], {
        queryParams: {
          create: true
        }
      })
    }
  })
}
