import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "../home/home.component";
import {RegFormComponent} from "../reg-form/reg-form.component";
import {RegGuard} from "../reg.guard";
import {ErrorPageComponent} from "../error-page/error-page.component";

const routes: Routes =[
  {path: '', component: HomeComponent},
  {path: 'registration', component: RegFormComponent, canActivate: [RegGuard]},
  {path: 'error', component: ErrorPageComponent},
  {path: '**', redirectTo: '/error'},
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
