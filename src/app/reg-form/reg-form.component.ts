import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {matchValidator} from "../my.validators";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {DataManagerService} from "../services/data-manager.service";


@Component({
  selector: 'app-reg-form',
  templateUrl: './reg-form.component.html',
  styleUrls: ['./reg-form.component.scss']
})
export class RegFormComponent implements OnInit {
  form: FormGroup


  constructor(private router: Router,
              private route: ActivatedRoute,
              private dataManagerService: DataManagerService) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      account: new FormGroup({
        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', [
          Validators.required,
          Validators.minLength(6),
          matchValidator.samePasswords('confirmPassword', true)
        ]),
        confirmPassword: new FormControl('', [
          Validators.required,
          matchValidator.samePasswords('password'),
        ])
      }),
      profile: new FormGroup({
        userName: new FormControl(''),
        phoneNumber: new FormControl('', [Validators.minLength(15)]),
        city: new FormControl('')
      }),
      company: new FormGroup({
        companyName: new FormControl('', [Validators.required]),
        propertyForm: new FormControl('', [Validators.required]),
        inn: new FormControl('', [
          Validators.required,
          Validators.minLength(9),
          Validators.pattern('^[0-9]+$')
        ]),
        okpo: new FormControl('', [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern('^[0-9]+$')
        ]),
        educationDate: new FormControl('')
      })
    })

    this.route.queryParams.subscribe((params: Params) => {
      if (!!params.create) {
        this.dataManagerService.hasFormData = !!params.create
      } else {
        // Continue form
        const companyFormGroup = this.form.get('company') as FormGroup;
        const companyData = this.dataManagerService.formData?.company
        if (companyData?.kpp) {
          companyFormGroup.addControl('kpp', new FormControl(''))
          companyFormGroup.controls.kpp.setValidators([
            Validators.required,
            Validators.minLength(9),
            Validators.pattern('^[0-9]+$')
          ]);
          companyFormGroup.controls.kpp.updateValueAndValidity()
        }

        const date: string = this.dataManagerService.formData?.company.educationDate
        if (date) {
          const [day, month, year] = date.split('.').map(Number);
          this.dataManagerService.formData.company.educationDate = new Date(year, month - 1, day);
        }

        // Create contacts form
        if (this.dataManagerService.formData?.contacts)
          this.dataManagerService.formData?.contacts.forEach(() => this.addContact())
        this.form.patchValue(this.dataManagerService.formData)
      }
    })
  }

  submit() {
    if (!this.form.valid) return
    this.saveFormData()

    delete this.dataManagerService.formData?.account?.confirmPassword
    // this.dataManagerService.formData = null
    this.dataManagerService.hasFormData = false
    this.form.reset()
    this.goHome()
  }

  // ============================== Account ==============================
  // Email ----------------------------------------------------------
  getErrorMessageEmail(): string {
    if (this.form.get('account.email').hasError('required')) return 'Введите email';
    return this.form.get('account.email').hasError('email') ? 'Некорректный email' : '';
  }

  // Password ----------------------------------------------------------
  hidePass = true

  getErrorMessagePassword(): string {
    const pass = this.form.get('account.password')
    if (pass.hasError('required')) return 'Введите пароль'
    else if (pass.hasError('minlength')) return 'Пароль короче 6 символов'
    return pass.hasError('password') ? '' : 'Ошибка';
  }

  // ============================== Profile ==============================
  // Phone ----------------------------------------------------------
  formatPhoneNumber(inputValue: string): string {
    const value = inputValue.replace(/[^0-9]/g, '');
    let format = '*(***) *** ****';
    for (let i = 0; i < value.length; i++) {
      format = format.replace('*', value.charAt(i));
    }
    if (format.indexOf('*') >= 0) {
      format = format.substring(0, format.indexOf('*'));
    }
    return format.trim();
  }

  onInputToProfile(event: any) {
    const formattedValue = this.formatPhoneNumber(event.target.value);
    this.form.get('profile.phoneNumber').setValue(formattedValue);
  }

  getErrorMessagePhone(): string {
    const phone = this.form.get('profile.phoneNumber')
    if (phone.hasError('minlength')) return 'Номер введен не полностью'
    return phone.hasError('phoneNumber') ? '' : 'Ошибка';
  }

  // ============================== Company ==============================
  // Company Name --------------------------------------------------------
  getErrorMessageCompanyName(): string {
    const companyName = this.form.get('company.companyName')
    if (companyName.hasError('required')) return 'Введите название'
    return companyName.hasError('companyName') ? '' : 'Ошибка';
  }

  // Property form ----------------------------------------------------------
  getErrorMessagePropertyForm(): string {
    const propertyForm = this.form.get('company.propertyForm')
    if (propertyForm.hasError('required')) return 'Выберите форму собственности'
    return propertyForm.hasError('propertyForm') ? '' : 'Ошибка';
  }

  setValidKPP() {
    const companyFormGroup = this.form.get('company') as FormGroup;
    if (this.form.get('company.propertyForm').value === 'entity') {
      companyFormGroup.addControl('kpp', new FormControl(''))
      companyFormGroup.controls.kpp.setValidators([
        Validators.required,
        Validators.minLength(9),
        Validators.pattern('^[0-9]+$')
      ]);
      companyFormGroup.controls.kpp.updateValueAndValidity()
    } else companyFormGroup.removeControl('kpp')
  }

  // INN ----------------------------------------------------------
  getErrorMessageINN(): string {
    const inn = this.form.get('company.inn')
    if (inn.hasError('required')) return 'Введите ИНН'
    else if (inn.hasError('pattern')) return 'ИНН должен содержать только цифры'
    else if (inn.hasError('minlength')) return 'Недостаточная длина ИНН'
    return inn.hasError('inn') ? '' : 'Ошибка';
  }

  // KPP ----------------------------------------------------------
  getErrorMessageKPP(): string {
    const kpp = this.form.get('company.kpp')
    if (kpp.hasError('required')) return 'Введите КПП'
    else if (kpp.hasError('pattern')) return 'КПП должен содержать только цифры'
    else if (kpp.hasError('minlength')) return 'Недостаточная длина КПП'
    return kpp.hasError('kpp') ? '' : 'Ошибка';
  }

  // OKPO ----------------------------------------------------------
  getErrorMessageOKPO(): string {
    const okpo = this.form.get('company.okpo')
    if (okpo.hasError('required')) return 'Введите ОКПО'
    else if (okpo.hasError('pattern')) return 'ОКПО должен содержать только цифры'
    else if (okpo.hasError('minlength')) return 'Недостаточная длина ОКПО'
    return okpo.hasError('okpo') ? '' : 'Ошибка';
  }

  // ============================== Contacts ==============================
  addContact() {
    if (!this.form.get('contacts'))
      this.form.addControl('contacts', new FormArray([]))
    const newContact: FormGroup = new FormGroup({
      name: new FormControl('', Validators.required),
      jobTitle: new FormControl('', Validators.required),
      phone: new FormControl('', [
        Validators.required,
        Validators.minLength(15)
      ])
    })
    this.contacts.push(newContact)
  }

  get contacts(): FormArray {
    return (this.form.get('contacts') as FormArray)
  }

  removeContact(idx: number) {
    this.contacts.removeAt(idx)
  }

  onInputToContacts(event: any, idx: number) {
    const formattedValue = this.formatPhoneNumber(event.target.value);
    this.contacts.at(idx).get('phone').setValue(formattedValue)
  }

  getErrorMessageNameInContacts(idx: number): string {
    const name = this.contacts.at(idx).get('name')
    if (name.hasError('required')) return 'Введите имя'
    return name.hasError('name') ? '' : 'Ошибка'
  }

  getErrorMessageJobTitlesInContacts(idx: number): string {
    const jobTitle = this.contacts.at(idx).get('jobTitle')
    if (jobTitle.hasError('required')) return 'Введите должность'
    return jobTitle.hasError('jobTitle') ? '' : 'Ошибка'
  }

  getErrorMessagePhoneInContacts(idx: number): string {
    const phone = this.contacts.at(idx).get('phone')
    if (phone.hasError('required')) return 'Введите номер'
    else if (phone.hasError('minlength')) return 'Номер введен не полностью'
    return phone.hasError('phones') ? '' : 'Ошибка';
  }

  // ============================== Other ==============================
  saveFormData() {
    const dateControl = this.form.get('company.educationDate')
    if (dateControl.value) dateControl.setValue(dateControl.value.toLocaleDateString())
    this.dataManagerService.formData = {...this.form.value}
  }

  goHome() {
    this.router.navigate(['/'])
    console.log('formData', this.dataManagerService.formData)
  }
}
