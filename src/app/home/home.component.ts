import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {DataManagerService} from "../services/data-manager.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  isActive: boolean = !this.dataManagerService.hasFormData

  constructor(private dataManagerService: DataManagerService,
              private router: Router) {}

  startReg() {
    this.router.navigate(['/registration'], {
      queryParams: {
        create: true
      }
    })
  }

  continueReg() {
    this.router.navigate(['/registration'])
  }
}
